import java.util.Scanner;
import java.util.Arrays;
import java.util.Comparator;

public class SensorsReadings {
    public static void main(String[] args) {
        Scanner sensors = new Scanner(System.in);
        System.out.print("Введите показания датчиков: ");
        String readings = sensors.nextLine();
        System.out.print("1 - сортировка по id / 2 - по температуре: ");
        int mode = sensors.nextInt();
        Integer[][] result = OutputReadings(readings);
        switch (mode) {
            case 1 -> {
                Arrays.sort(result, Comparator.comparingInt(o -> o[0]));
                for (int i = 0; i < result.length; i++)
                    System.out.println(result[i][0] + " " + result[i][1]);
            }
            case 2 -> {
                Arrays.sort(result, Comparator.comparingInt(o -> o[1]));
                for (int i = 0; i < result.length; i++)
                    System.out.println(result[i][0] + " " + result[i][1]);
            }
        }
    }
    public static Integer[][] OutputReadings(String readings) {
        String[] text = readings.split("@");
        Integer[][] output = new Integer[text.length][2];
        for (int i = 0; i < text.length; i++){
            output[i][0] = Integer.parseInt(text[i].substring(0, 2));
            output[i][1] = Integer.parseInt(text[i].substring(2, text[i].length()));
        }
        return output;
    }
}

