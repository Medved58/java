import java.util.Scanner;

public class SubstringSearch {
    public static void main(String[] args) {
        Scanner stringScanner = new Scanner(System.in);
        System.out.print("Введите строки черз пробел: ");
        String[] str = stringScanner.nextLine().split(" ");
        System.out.print("Введите подстроку: ");
        String substr = stringScanner.nextLine();
        System.out.println("Кол-во подстрок: " + SearchTotal(str,substr));
    }

    public static int SearchTotal(String[] strings, String sub){
        int count = 0;
        int i = -1;
        for (String str: strings){
            while ((i = str.indexOf(sub, i+1)) > -1) ++count;
        }
        return count;
    }
}
