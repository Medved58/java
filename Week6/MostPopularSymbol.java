import java.util.Arrays;
import java.util.Scanner;

public class MostPopularSymbol {

    public static void main(String[] args) {
        Scanner idk = new Scanner(System.in);
        System.out.print("Введите текст: ");
        String text = idk.nextLine();
        char[] arr = text.toCharArray();
        Arrays.sort(arr);
        int count = 1;
        int max = 0;
        char c = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == arr[i - 1]) {
                count++;
                if (count > max) {
                    c = arr[i];
                    max = count;
                }
            } else {
                count = 1;
            }
        }
        if (max > 1)
            System.out.println("Символ " + c + " повторяется " + max + " раз(а).");
    }
}