import java.util.Scanner;

public class Task1_c {
    public static void main(String[] args) {
        Scanner weatherScanner = new Scanner(System.in);
        System.out.print("Температура (жарко/тепло/холодно): ");
        String temperature = weatherScanner.nextLine();
        switch(temperature){
            case"тепло"-> {
                System.out.print("Осадки (ясно/облачно/дождь/снег/град): ");
                String precipitations = weatherScanner.nextLine();
                switch(precipitations){
                    case"ясно", "облачно"-> {
                        System.out.print("Ветер (есть/нет): ");
                        String wind = weatherScanner.nextLine();
                        switch(wind){
                            case"нет" -> {
                                System.out.print("Влажность (высокая/низкая): ");
                                String humidity = weatherScanner.nextLine();
                                switch(humidity){
                                    case"низкая" -> {
                                        System.out.println("Да.");
                                    }
                                    default -> System.out.println("Нет.");
                                }
                            }
                            default -> System.out.println("Нет.");
                        }
                    }
                    default -> System.out.println("Нет.");
                }
            }
            default -> System.out.println("Нет.");
        }
    }
}