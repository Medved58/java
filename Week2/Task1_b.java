import java.util.Scanner;

public class Task1_b {
    public static void main(String[] args) {
        Scanner idk = new Scanner(System.in);
        System.out.print("Введите номер месяца: ");
        int month = idk.nextInt();
        System.out.print("Введите год: ");
        int year = idk.nextInt();
        boolean isLeap;
        if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0))) {
            isLeap = true;
        }
        else {
            isLeap = false;
        }
        switch (month) {
            case 1, 3, 5, 7, 8, 10, 12 -> {
                System.out.println("В данном месяце 31 день.");
            }
            case 4, 6, 9, 11 -> {
                System.out.println("В данном месяце 30 дней.");
            }
            case 2 -> {
                if (isLeap == true) {
                    System.out.println("В данном месяце 29 дней.");
                }
                else if (isLeap == false) {
                    System.out.println("В данном месяце 28 дней.");
                }
            }
        }
    }
}