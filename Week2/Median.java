import java.util.Arrays;
import java.util.Scanner;

public class Median {
    public static void main(String[] args) {
        try (Scanner idk = new Scanner(System.in)) {
            System.out.print("Введите массив(через пробел): ");
            String nums = idk.nextLine();
            String[] StringNumbers = nums.split(" ");
            double[] numbers = new double[StringNumbers.length];
            for (int i = 0; i < StringNumbers.length; i++) {
                numbers[i] = Double.parseDouble(StringNumbers[i]);
            }
            System.out.println("Медиана набора чисел: " + median(numbers));

        }

    }
    private static double median(double[] total) {
        double j;
        Arrays.sort(total);
        int size = total.length;
        if(size % 2 == 1){
            j = total[(size-1)/2];
        }
        else {
            j = (total[size/2-1] + total[size/2])/2;
        }
        return j;
    }
}

