import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Dictionary {
    public static void main(String[] args) {
        try (Scanner idk = new Scanner(System.in)) {
            System.out.println("Введите словарь: ");
            int n = Integer.parseInt(idk.nextLine());
            Map<String, ArrayList<String>> inputDict = new HashMap<>();

            for (int i = 0; i < n; i++) {
                String[] inputString = idk.nextLine().split(",? ");
                ArrayList<String> inputArray = new ArrayList<>();
                for (int k = 1; k < inputString.length; k++) {
                    inputArray.add(inputString[k]);
                    inputArray.remove("-");
                }
                inputDict.put(inputString[0], new ArrayList<>(inputArray));
            }

            Map<String, ArrayList<String>> outputDict = new TreeMap<>(Transform(inputDict));
            System.out.println(outputDict.size());
            outputDict.forEach((k, v) -> System.out.println((k + " - " + String.join(", ", v))));
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static Map<String, ArrayList<String>> Transform(Map<String, ArrayList<String>> data) {
        Map<String, ArrayList<String>> outputDict = new HashMap<>();
        for (Map.Entry<String, ArrayList<String>> entry : data.entrySet()) {
            ArrayList<String> words = new ArrayList<>(entry.getValue());
            for (String word : words) {
                ArrayList<String> temp;
                if (outputDict.containsKey(word)) {
                    temp = outputDict.get(word);
                    temp.add(entry.getKey());
                } else {
                    temp = new ArrayList<>();
                    temp.add(entry.getKey());
                    outputDict.put(word, temp);
                }
            }
        }

        return outputDict;
    }
}
