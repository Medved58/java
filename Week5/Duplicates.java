import java.util.Collections;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashSet;


public class Duplicates {
    public static void main(String[] args){
        try (Scanner idk = new Scanner(System.in)) {
            System.out.print("Введите элементы входного массива(через пробел): ");
            String input = idk.nextLine();
            String[] inputArray = input.split(" ");
            ArrayList<String> list = new ArrayList<>();
            Collections.addAll(list, inputArray);

            HashSet<String> set = new HashSet<>(list);
            System.out.print("Массив без дубликатов: " + set);
        }
        catch (NumberFormatException e) {
            System.out.println(e.getClass());
        }
    }
}

