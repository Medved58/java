import java.util.ArrayList;
import java.util.Scanner;

public class Water {
    public static void main(String[] args) {
        try (Scanner idk = new Scanner(System.in)) {
            System.out.print("Введите числа через запятую: ");
            String[] input = idk.nextLine().split(",");
            ArrayList<Integer> arraylist = new ArrayList<>();
            for (String s : input) arraylist.add(Integer.parseInt(s));
            System.out.println("Объем: " + SizeContainer(arraylist, arraylist.size()-1));
        }
    }
    public static int SizeContainer(ArrayList<Integer> list, int height) {
        int x = 0, y = height, temp;
        int width = y - x;
        int size = width * Math.min(list.get(y), list.get(x));
        while (x < y) {
            if (list.get(x) < list.get(y)) {
                x++;
            }
            else {
                y--;
            }
            width = y - x;
            temp = width * Math.min(list.get(y), list.get(x));
            if (temp > size) size = temp;
        }
        return size;
    }
}
