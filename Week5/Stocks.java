import java.util.ArrayList;
import java.util.Scanner;

public class Stocks {
    public static void main(String[] args) {
        try (Scanner idk = new Scanner(System.in)) {
            System.out.print("Введите числа через запятую: ");
            String[] input = idk.nextLine().split(",");
            ArrayList<Integer> prices = new ArrayList<>();
            for (String s : input) prices.add(Integer.parseInt(s));
            System.out.println("Прибыль: " + SearchMaxProfit(prices));
        } catch (NumberFormatException e) {
            System.out.println("Ошибка ввода, проверьте данные");
        }
    }
    public static Integer SearchMaxProfit(ArrayList<Integer> list) {
        int max = 1;
        int min = 0;
        int MaxProfit = 0;
        while (max < list.size()) {
            if (list.get(min) < list.get(max)) {
                int profit = list.get(max) - list.get(min);
                MaxProfit = Math.max(profit, MaxProfit);
            }
            else {
                min = max;
            }
            max++;
        }
        return MaxProfit;
    }
}

