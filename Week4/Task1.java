import java.io.*;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        try {
            String path = ("C://Test.txt");
            File test = new File(path);
            Scanner idk = new Scanner(test);
            while(idk.hasNextLine()) {
                System.out.println(idk.nextLine());
            }
            idk.close();
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }
}